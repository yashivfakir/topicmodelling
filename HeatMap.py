import matplotlib.pyplot as plt
import numpy as np

#here's our data to plot, all normal Python lists
x = [0,1, 2, 3, 4, 5]
y = [0.1, 0.2, 0.3, 0.4, 0.5]
temp=[[1 ,2 ,3 ,4, 5],[1, 2, 3, 4, 5],[1, 2 ,3 ,4 ,5],[1 ,2 ,3 ,4 ,5],[1 ,2 ,3 ,4 ,5]]


intensity = [
    [5, 10, 15, 20, 25],
    [30, 35, 40, 45, 50],
    [55, 60, 65, 70, 75],
    [80, 85, 90, 95, 100],
    [105, 110, 115, 120, 125]
]

#setup the 2D grid with Numpy
x ,x = np.meshgrid(x,x)
print(x)


#convert intensity (list of lists) to a numpy array for plotting
intensity = np.array(intensity)


#now just plug the data into pcolormesh, it's that easy!
plt.pcolormesh(x, x,temp)
plt.colorbar() #need a colorbar to show the intensity scale
plt.show() #boom