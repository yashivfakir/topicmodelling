import requests

#gets articles from News24
url =('https://newsapi.org/v2/everything?sources=news24&apiKey=ad7286dd5ed44137a5e83fd1995050d0')

ArticleData= requests.get(url).json()


#Empty array to store headlines from the articles
RawDataArray=[len(ArticleData['articles'])]



for x in range(0,len(ArticleData['articles'])):
    temp = ArticleData['articles'][x]

    newTemp = temp.get('title').encode('utf-8').strip()

    RawDataArray.append(newTemp.decode('utf-8'))



#index [0] is the length and am thus removing it AND removed the preTitle e.g. "LIVE:bla bla bla'
del RawDataArray[0]
HeadLineArray=[]
for i in RawDataArray:
    i=i[12:len(i)]
    HeadLineArray.append(i)


#function to retrieve HeadlineArray
def getArray():
    return HeadLineArray

