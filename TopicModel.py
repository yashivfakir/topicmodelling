import News24API

Array= News24API.getArray()

#Cleans the Array.........................................................................................

from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import string
stop = set(stopwords.words('english'))
exclude = set(string.punctuation)
lemma = WordNetLemmatizer()
def clean(doc):
    stop_free = " ".join([i for i in doc.lower().split() if i not in stop])
    punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
    normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
    return normalized

CleanedArray = [clean(doc).split() for doc in Array]



#Preparing Array-Term Matrix...................................................................................
# Importing Gensim
import gensim
from gensim import corpora

# Creating the term dictionary of our courpus, where every unique term is assigned an index.
dictionary = corpora.Dictionary(CleanedArray)
# Converting list of Array (corpus) into Array Term Matrix using dictionary prepared above.
doc_term_matrix = [dictionary.doc2bow(doc) for doc in CleanedArray]

#Running LDA Model...............................................................................................
# Creating the object for LDA model using gensim library
Lda = gensim.models.ldamodel.LdaModel

# Running and Trainign LDA model on the Array term matrix.

ldamodel = Lda(doc_term_matrix, num_topics=10, id2word = dictionary, passes=50)


#Topic Results........................................................................................................
#formats the output to isolate on the headline data, then adds to TopicsArray[]
StringTopicsArray=[]
for index, topic in ldamodel.show_topics(formatted=False, num_words=10):
    StringTopicsArray.append(format( [w[0] for w in topic]))


TopicArray=[]
count=0
for x in StringTopicsArray:
    x=x[1:len(x)-1]
    temp=x.split(",")
    TopicArrayElement = []
    for y in temp:
        ind=y.index("'")
        y=y[ind+1:len(y)-1]
        TopicArrayElement.append(y)
    TopicArray.append(TopicArrayElement)



#Heat Map data points...................................................................................................
#formats the output to heatplot data, then adds to HeatPlotDataArray
StringHeatPlotDataArray=[]
for index, number in ldamodel.show_topics(formatted=False, num_words=10):
    StringHeatPlotDataArray.append('{}'.format([w[1] for w in number]))


HeatPlotDataArray=[]
count=0
for x in StringHeatPlotDataArray:
    x=x[1:len(x)-1]
    temp=x.split(",")
    HeatElement = []
    for y in range(len(temp)):
        if y==0:
            HeatElement.append(temp[y])
        else:
            temp[y]=temp[y][1:len(temp[y])]
            HeatElement.append(temp[y])
    HeatPlotDataArray.append(HeatElement)

print(HeatPlotDataArray)

#Methods................................................................................................................

#Method to retrieve the TopicsArray
def getTopicsArray():
    return TopicArray

def getHeatPlotDataArray():
    return HeatPlotDataArray


